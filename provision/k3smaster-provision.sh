#!/usr/bin/env bash

apt-get update
echo "### Installing docker dependncies"
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
echo "### Add docker repository"
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
echo "### Policy docker"
apt-cache policy docker-ce
echo "### Installing docker"
sudo apt install docker-ce -y

echo "### Docker daemon status"
sudo systemctl status docker

echo "### Add vagrant to docker group"
sudo usermod -aG docker vagrant

COUNTRY=CA
STATE=ON
CITY=Toronto
ORG=CETBIZ
DEPT=DEVOPS
WEBSITE="cetbiz.com"
EMAIL="info@cetbiz.com"

echo "### Download file from github to generate certificates"
sudo -- bash -c "mkdir /data \
                 && curl https://raw.githubusercontent.com/kekru/linux-utils/master/cert-generate/create-certs.sh -o /data/create-certs.sh \
                 && chmod +x /data/create-certs.sh \
                 && sed -i -e s#CASUBJSTRING=.*#CASUBJSTRING=\"/C=$COUNTRY/ST=$STARE/L=$CITY/O=$ORG/OU=$DEPT/CN=$WEBSITE/emailAddress=$EMAIL\"#  /data/create-certs.sh"

echo "### Generate server and client certificates"
HOSTNAME="Sameers-MacBook-Pro-pesonal.local"
sudo -- bash -c "cd /data \
                 && ./create-certs.sh -m ca -pw password -t certs -e 900 \
                 && ./create-certs.sh -m server -h k3smaster -hip 10.0.0.120 -pw password -t certs -e 365 \
                 && ./create-certs.sh -m client -h $HOSTNAME -pw password -t certs -e 365 "

echo "### Create docker daemon.json file"
sudo sed -i 's#-H fd://##' /lib/systemd/system/docker.service
sudo -- bash -c 'cat <<EOF >>/etc/docker/daemon.json
{
  "hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2376", "fd://"],
  "tls": true,
  "tlscacert": "/data/certs/ca.pem",
  "tlscert": "/data/certs/server-cert.pem",
  "tlskey": "/data/certs/server-key.pem",
  "tlsverify": true,
  "insecure-registry": ["10.96.0.0/12"]
}
EOF'

# Restart docker daemon and ensure service is running as expected.
sudo systemctl daemon-reload
sudo systemctl restart docker

