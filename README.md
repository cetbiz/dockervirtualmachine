# Use virtual machine as docker container agent 
Docker desktop has some known limitations. Refer for more details on [Docker Desktop](https://www.docker.com/pricing/faq). 
Couple important limitations are, 
1. Docker Desktop stores Linux containers and images in a single, large “disk image” file in the Mac filesystem. 
   This is different from Docker on Linux, which usually stores containers and images in the /var/lib/docker directory.
2. Docker Desktop requires a per user paid Pro, Team or Business subscription for professional use in larger companies 
   with subscriptions available for as little as $5 per user, per month.

## Alternative 
Setup a virtual machine with docker socket exposed on TCP/IP. SSL certificates can be configured to secure TCP/IP socket
for docker. To provision virtual machine, automating docker installation and configuration we are leveraging Vagrant.   

## Vagrant 
Vagrant provisions virtual machines on multiple platforms like VirtualBox, Parallel, VMWare etc. 
Vagrant allows to automate the Virtual machine provisioning as well as application configuration post provisioning.
[Vagrantfile](https://www.vagrantup.com/docs/vagrantfile) contains all required configuration to provision a Virtual machine. 

More about vagrant,
https://www.vagrantup.com/intro
https://www.vagrantup.com/docs

## My setup while writing this document
```bash
vagrant version
Installed Version: 2.2.19
Latest Version: 2.2.19

VirtualBox -h
Oracle VM VirtualBox VM Selector v6.1.30
(C) 2005-2021 Oracle Corporation
All rights reserved.
```

## Host Networking 
My home router is using CIDR 10.0.0.0/24.   

Host machine (laptop) IP address:  en0: 10.0.0.54
Virtual machine IP address: 
interface 1: 10.0.0.120

You need to modify values in Vagrantfile according to your network CIDR and SSID or Network name.
`:ip => "10.0.0.120", :bridge => "en0: Wi-Fi (AirPort)"`

Tip: Network Name can be found on Wi-Fi settings on Mac. 

## Commands to provision Virtual machine. 
Execute command from your terminal to provision Virtual machine. 
```bash
vagrant up
```
Vagrant downloads [BOX](https://www.vagrantup.com/docs/boxes) which is ready to use Virtual machine image. 
This image is then used to create Virtual machine. SSH key will be provisioned and added to Virtual machine
making it easy to access. 

To login into Virtual machine execute command,
```bash
vagrant ssh 
```
If successful you will be logged-in into Virtual Machine. Now you can verify if docker is configured as expected. 


## Test docker socket
Once Virtualbox provisioned successfully docker socket can be reachable over tcp/ip. 
It can be verified by telnet. 

```bash
telnet 10.0.0.120 2376
Trying 10.0.0.120...
Connected to 10.0.0.120.
Escape character is '^]'.
^CConnection closed by foreign host.
```

## Setup local machine
You need to copy Client SSL certificate, key and CA.pem file from virtual machine to local machine/laptop. 
Once downloaded docker client can be configured to use it. 

## Copy client certificates to desired directory on your laptop
All commands should be run on your local/host machine. 
```bash
vagrant plugin install vagrant-scp
 
mkdir -p ~/.k3svagrant/certs
vagrant scp :/data/certs/ca.pem ~/.k3svagrant/certs
vagrant scp :/data/certs/client-Sameers-MacBook-Pro-pesonal.local-key.pem  ~/.k3svagrant/certs
vagrant scp :/data/certs/client-Sameers-MacBook-Pro-pesonal.local-cert.pem  ~/.k3svagrant/certs
```

## Directory should look like this,
```bash
➜  certs ll ~/.k3svagrant/certs
total 24
drwxr-xr-x  3 sonaikar  staff    96 24 Jul 16:02 ..
-r--------@ 1 sonaikar  staff  1883 25 Jul 09:16 client-Sameers-MacBook-Pro-pesonal.local-cert.pem
-r--------@ 1 sonaikar  staff  3243 25 Jul 09:16 client-Sameers-MacBook-Pro-pesonal.local-key.pem
-r--------@ 1 sonaikar  staff  1996 25 Jul 09:16 ca.pem
drwxr-xr-x  5 sonaikar  staff   160 25 Jul 09:19 .


# Rename cert and key files
mv client-Sameers-MacBook-Pro-pesonal.local-cert.pem cert.pem
mv client-Sameers-MacBook-Pro-pesonal.local-key.pem key.pem
```

## Create a file on laptop to set docker host connection
```bash
➜  k3svagrant cat k3sdocker.sh
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://10.0.0.120:2376"
export DOCKER_CERT_PATH="/Users/sonaikar/.k3svagrant/certs"
```

## Source the file in environment.
```bash 
source k3sdocker.sh
```

## You can see docker processes running
```bash
docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## Create an alias in .bashrc/.zshrc file to load docker environment
```bash
alias k3sdocker='source ~/k3svagrant/k3sdocker.sh'
```

# Manual provisioning details 
Refer to docs/01-PrepareVirtualMachine.md for more details on provisioning. 
